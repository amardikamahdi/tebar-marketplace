import {Raleway} from "next/font/google"
import Hero from "@/components/Hero"
import NavigationBar from "@/components/Navigation-Bar"
import Banner from "@/components/Home/Banner"
import ArrivalCard from '@/components/Home/ArrivalCard'
import SecondBanner from '@/components/Home/SecondBanner'
import Categories from '@/components/Home/Categories'
import BestSeller from "@/components/Home/BestSeller"
import BrandWeLove from "@/components/Home/BrandWeLove"
import Footer from '@/components/Footer'

const raleway = Raleway({
    subsets: ['latin'],
    display: 'swap',
});

export default function Home() {
    return (
        <>
            <div className={raleway.className}>
                <NavigationBar/>
                <Hero/>
                <Banner/>
                <ArrivalCard/>
                <SecondBanner/>
                <Categories/>
                <BestSeller/>
                <BrandWeLove/>
                <Footer/>
            </div>
        </>
    )
}
