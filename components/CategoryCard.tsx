import {Lexend, Raleway} from "next/font/google";
import Image, {StaticImageData} from "next/image";

const lexend = Lexend({
    subsets: ['latin'],
})

const raleway = Raleway({
    subsets: ['latin'],
})

interface Category {
    name: string
    image: StaticImageData
}

const CategoryCard = (props: Category) => {
    return (
        <>
            <button
                className="flex-col items-center w-full text-white p-4 bg-[#C5023B]">
                <Image className={'mx-auto h-[50px] w-[40px]'} src={props.image} alt={''}/>
                <p>{props.name}</p>
            </button>
        </>
    )
}

export default CategoryCard