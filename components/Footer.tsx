import Image from "next/image";
import Logo from '@/public/Main-Logo.svg'

const Footer = () => {
    return (
        <>
            <footer className={'w-full max-w-full bg-white p-10'}>
                <Image src={''} alt={''}/>
                <div className="container mx-auto w-3/4">
                    <div className="flex w-full gap-6">
                        <div className="w-1/4">
                            <Image className={'mb-8'} src={Logo} alt={''}/>
                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                nulla pariatur. Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod
                                tempor.</p>
                        </div>
                        <div className="w-1/4">
                            <div className={'border-l-8 border-[#C70039]'}>
                                <p className={'ml-4 text-[20px] font-bold'}>Kategori</p>
                            </div>
                            <ul className={'ml-5 mt-5 font-medium'}>
                                <li className={'my-1'}>Accessories</li>
                                <li className={'my-1'}>Bags</li>
                                <li className={'my-1'}>Fashion</li>
                                <li className={'my-1'}>Footwear</li>
                                <li className={'my-1'}>Lifestyle</li>
                                <li className={'my-1'}>Outerwear</li>
                            </ul>
                        </div>
                        <div className="w-1/4">
                            <div className={'border-l-8 border-[#C70039]'}>
                                <p className={'ml-4 text-[20px] font-bold'}>Pintasan</p>
                            </div>
                            <ul className={'ml-5 mt-5 font-medium'}>
                                <li className={'my-1'}>Home</li>
                                <li className={'my-1'}>Shop</li>
                                <li className={'my-1'}>About</li>
                                <li className={'my-1'}>Blog</li>
                                <li className={'my-1'}>Contact</li>
                                <li className={'my-1'}>Cart</li>
                                <li className={'my-1'}>My Account</li>
                            </ul>
                        </div>
                        <div className="w-1/4">
                            <div className={'border-l-8 border-[#C70039]'}>
                                <p className={'ml-4 text-[20px] font-bold'}>Alamat</p>
                            </div>
                            <p className={'ml-5 mt-5 font-medium text-[15px]'}>Jalan Palem Nomor 1, Cemani RT 05 RW 09,
                                Kecamatan Grogol, Kabupaten Sukoharjo, Provinsi Jawa Tengah 57552</p>
                        </div>
                    </div>
                </div>
            </footer>
            <div className="container bg-[#C70039] w-full py-[30px]">
                <p className={'text-center text-white'}>© Copyright {new Date().getFullYear()} <span
                    className={'font-bold'}>Olzhop</span>.
                    All Rights Reseved.</p>
            </div>
        </>
    )
}

export default Footer;