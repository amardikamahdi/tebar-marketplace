import Image, {StaticImageData} from "next/image";
import PlusIcon from '@/public/plus-icon.svg'
import StarIcon from '@/public/star-icon.svg'
import EmptyStarIcon from '@/public/empty-star-icon.svg'
import {Raleway} from 'next/font/google'

const raleway = Raleway({
    subsets: ['latin'],
});

interface cardProps {
    title: string;
    price: number;
    star: number;
    is_sale: boolean;
    discount: number;
    img: StaticImageData;
}

const Card = (props: cardProps) => {
    const formatter = new Intl.NumberFormat('id-ID', {style: 'currency', currency: 'IDR', maximumFractionDigits: 0})

    return (
        <>
            <div className="w-full bg-white my-10">
                <div className={'relative'}>
                    {props.is_sale && (
                        <div
                            className="flex absolute left-4 z-10 top-4 h-8 w-16 bg-[#C70039]">
                            <p className={`${raleway.className} text-white my-auto mx-auto`}>Sale!</p>
                        </div>
                    )}
                    <img
                        className="object-cover w-full h-full"
                        src={props.img.src}
                        alt="avatar"/>
                    <div className={'group'}>
                        <div
                            className="flex absolute left-0 top-0 h-full w-full bg-transparent transition-all duration-500 group-hover:bg-[#C70039] group-hover:opacity-90">
                            <Image
                                className={'my-auto mx-auto h-auto w-auto transition-all duration-500 opacity-0 group-hover:opacity-100'}
                                src={PlusIcon}
                                alt={''}/>
                        </div>
                    </div>
                </div>

                <div className="p-5 text-center">
                    <a href="#" className="block text-[19px] text-xl font-bold text-gray-800"
                       role="link">{props.title}</a>
                    <div className={'flex items-center mx-auto w-1/2 my-2'}>
                        {props.star >= 1 && props.star <= 5 && (
                            <>
                                {[...Array(props.star)].map((_, index) => (
                                    <Image className={'w-1/5 h-auto'} src={StarIcon} alt={''} key={index}/>
                                ))}
                                {[...Array(5 - props.star)].map((_, index) => (
                                    <Image className={'w-1/5 h-auto'} src={EmptyStarIcon} alt={''} key={index}/>
                                ))}
                            </>
                        )}
                    </div>

                    <div className="block">
                        {props.is_sale && (
                            <span
                                className="text-lg text-[#666] font-medium line-through mr-1">{formatter.format(props.price)}</span>
                        )}
                        <span
                            className="text-lg font-medium text-gray-800">{formatter.format(props.price - ((props.price * props.discount) / 100))}</span>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Card;