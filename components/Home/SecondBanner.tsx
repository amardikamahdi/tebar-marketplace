import Image from "next/image";
import Banner from '@/public/price.png'
import {Lexend, Raleway} from "next/font/google";

const lexend = Lexend({
    subsets: ['latin'],
})

const raleway = Raleway({
    subsets: ['latin'],
});

const SecondBanner = () => {
    return (
        <>
            <div className="container w-full max-w-full p-10">
                <div className="relative w-3/4 mx-auto">
                    <button
                        className="absolute w-2/6 top-[-5%] right-[-2.5%] text-white p-10 bg-[#C5023B]">
                        <p className={`${lexend.className} font-bold uppercase text-[60px]`}>80% off</p>
                        <p
                            className={`${raleway.className} text-[20px] text-left`}>Outerwear Fashion</p>
                        <button
                            className="absolute bottom-[-10%] right-[7.5%] px-8 py-3 font-bold font-small tracking-wide text-white capitalize transition-all duration-300 transform bg-[#FF5733] hover:pr-10">
                            Shop Now
                        </button>
                    </button>
                    <button
                        className="absolute w-1/5 bottom-[-5%] left-[-2.5%] text-white font-bold text-left p-10 bg-[#FF5733]">
                        <p className={`${lexend.className} uppercase text-[30px]`}>PRICE MATCH</p>
                        <p
                            className={`${raleway.className} text-[20px]`}>Guarantee</p>
                    </button>
                    <Image className={'w-full min-h-[360px] max-h-[360px] mx-auto z-0'} src={Banner} alt={''}/>
                </div>
            </div>
        </>
    )
}
export default SecondBanner;