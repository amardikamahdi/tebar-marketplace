import Card from '@/components/Card'
import {Lexend} from "next/font/google"
import Product1 from "@/public/product1.png";
import Product2 from "@/public/product2.png";
import Product3 from "@/public/product3.png";


const lexend = Lexend({
    subsets: ['latin'],
})

const BestSeller = () => {
    return (
        <>
            <div className={'w-full max-w-full my-20'}>
                <div className={'container mx-auto w-3/4'}>
                    <div className={`${lexend.className} w-full`}>
                        <h1 className={'text-[55px] font-bold'}>Best Deals</h1>
                        <div className="flex gap-6">
                            <Card title={'Sunglasses'} price={100000} star={5} is_sale={true} discount={20}
                                  img={Product1}/>
                            <Card title={'Sunglasses'} price={100000} star={5} is_sale={true} discount={20}
                                  img={Product2}/>
                            <Card title={'Sunglasses'} price={100000} star={5} is_sale={true} discount={20}
                                  img={Product3}/>
                            <Card title={'Sunglasses'} price={100000} star={5} is_sale={true} discount={20}
                                  img={Product1}/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default BestSeller