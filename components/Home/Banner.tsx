import Banner1 from '@/public/Banner1.svg'
import Banner2 from '@/public/Banner2.svg'
import Image from "next/image";

const Banner = () => {
    return (
        <>
            <div className="container mx-auto">
                <div className="flex xl:flex-row flex-col items-center justify-around xl:mx-48 xl:my-14">
                    <div className={'relative m-10 xl:m-0'}>
                        <button
                            className="absolute bottom-[-7.5%] left-12 px-8 py-3 font-bold font-small tracking-wide text-white capitalize transition-all duration-300 transform bg-orange-600 hover:bg-orange-500 hover:pr-10">
                            Shop Now
                        </button>
                        <Image src={Banner1} alt={''}/>
                    </div>
                    <div className="relative m-10 xl:m-0">
                        <button
                            className="absolute bottom-[-7.5%] left-12 px-8 py-3 font-bold font-small tracking-wide text-white capitalize transition-all duration-300 transform bg-red-800 hover:bg-red-700 hover:pr-10">
                            Shop Now
                        </button>
                        <Image src={Banner2} alt={''}/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Banner;