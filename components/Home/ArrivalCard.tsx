import BgProductCard from '@/public/bg-product-card.png'
import {Lexend} from 'next/font/google'
import Card from "@/components/Card";
import Product1 from '@/public/product1.png'
import Product2 from '@/public/product2.png'
import Product3 from '@/public/product3.png'

const lexend = Lexend({
    subsets: ['latin'],
});

const ArrivalCard = () => {
    return (
        <>
            <div className={`${lexend.className} mt-20 mb-8`}>
                <div
                    className={'h-full w-full max-w-full bg-[#f4f4f4]'}
                    style={{
                        backgroundImage: `url(${BgProductCard.src})`,
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'right -60% bottom 0px',
                    }}>
                    <div className="container mx-auto h-auto p-8 w-4/5 max-w-4/5">
                        <h1 className={`text-[55px] font-bold text-[#1a243f] d-block`}>
                            New Arrivals
                        </h1>
                        <div className="flex flex-1 w-3/4 h-3/4 gap-14">
                            <Card title={'Sunglasses'} price={100000} star={5} is_sale={true} discount={20}
                                  img={Product1}/>
                            <Card title={'New Golden Sandals'} price={199000} star={3} is_sale={false} discount={0}
                                  img={Product2}/>
                            <Card title={'Blue Navy Bag'} price={160000} star={2} is_sale={false} discount={0}
                                  img={Product3}/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ArrivalCard;