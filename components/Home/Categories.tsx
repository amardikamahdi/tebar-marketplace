import {Lexend} from "next/font/google"
import CategoryCard from "@/components/CategoryCard"
import AccessoriesIcon from '@/public/accessories-icon.svg'
import OuterwearIcon from '@/public/outerwear-icon.svg'
import BagsIcon from '@/public/bags-icon.svg'
import LifestyleIcon from '@/public/lifestyle-icon.svg'
import FashionIcon from '@/public/fashion-icon.svg'
import FootwearIcon from '@/public/footwear-icon.svg'

const lexend = Lexend({
    subsets: ['latin'],
})

const Categories = () => {
    return (
        <>
            <div className={'w-full max-w-full my-10'}>
                <div className={'container mx-auto w-3/4'}>
                    <div className={`${lexend.className} w-full`}>
                        <h1 className={'text-[55px] font-bold mb-10'}>Categories</h1>
                        <div className="flex gap-6">
                            <CategoryCard name={'Accessories'} image={AccessoriesIcon}/>
                            <CategoryCard name={'Bags'} image={BagsIcon}/>
                            <CategoryCard name={'Fashion'} image={FashionIcon}/>
                            <CategoryCard name={'Footwear'} image={FootwearIcon}/>
                            <CategoryCard name={'Lifestyle'} image={LifestyleIcon}/>
                            <CategoryCard name={'Outerwear'} image={OuterwearIcon}/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Categories