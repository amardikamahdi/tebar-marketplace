import {Lexend} from "next/font/google"
import Image from "next/image";
import Brand1 from '@/public/Brand-01.png'
import Brand2 from '@/public/Brand-02.png'
import Brand3 from '@/public/Brand-03.png'
import Brand4 from '@/public/Brand-04.png'
import Brand5 from '@/public/Brand-05.png'
import Brand6 from '@/public/Brand-06.png'

const lexend = Lexend({
    subsets: ['latin'],
})

const BrandWeLove = () => {
    return (
        <>
            <div className={'w-full max-w-full bg-[#f4f4f4] my-20 py-[52px]'}>
                <div className={'container mx-auto w-3/4'}>
                    <div className={`${lexend.className} w-full`}>
                        <h1 className={'text-[55px] font-bold mb-4'}>Brand We Love</h1>
                        <div className="flex">
                            <Image
                                className={'bg-white'}
                                width={170}
                                height={75}
                                src={Brand1}
                                alt={''}/>
                            <Image
                                className={'bg-white'}
                                width={170}
                                height={75}
                                src={Brand2}
                                alt={''}/>
                            <Image
                                className={'bg-white'}
                                width={170}
                                height={75}
                                src={Brand3}
                                alt={''}/>
                            <Image
                                className={'bg-white'}
                                width={170}
                                height={75}
                                src={Brand4}
                                alt={''}/>
                            <Image
                                className={'bg-white'}
                                width={170}
                                height={75}
                                src={Brand5}
                                alt={''}/>
                            <Image
                                className={'bg-white'}
                                width={170}
                                height={75}
                                src={Brand6}
                                alt={''}/>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default BrandWeLove