import Image from "next/image";
import Logo from '@/public/logo-light.svg'
import Cart from '@/public/cart.svg'
import Search from '@/public/search.svg'
import {useEffect, useState} from "react";

const NavigationBar = () => {
    const [isAtTop, setIsAtTop] = useState(true)

    useEffect(() => {
        function handleScroll() {
            if (window.scrollY > 0) {
                setIsAtTop(false)
            } else {
                setIsAtTop(true)
            }
        }

        window.addEventListener('scroll', handleScroll)
        return () => window.removeEventListener('scroll', handleScroll)
    }, [])

    return (
        <>
            <nav
                className={`transition-all duration-300 ${isAtTop ? 'relative bg-[#900C3E]' : 'sticky top-0 z-50 bg-[#C70039]'}`}>
                <div
                    className={`transition-all duration-300 container mx-auto w-3/4 max-w-3/4 ${isAtTop ? 'px-8 py-6' : 'px-8 py-3'}`}>
                    <div className="flex items-center w-full justify-between">
                        <a href="#">
                            <Image className={`w-auto transition-all duration-300 ${isAtTop ? 'h-11' : 'h-10'}`}
                                   src={Logo} alt={''}/>
                        </a>

                        <div className="flex">
                            <div className="flex items-center gap-6 mx-4">
                                <a href="" className={'sub-nav-text'}>Home</a>
                                <a href="" className={'sub-nav-text'}>Shop</a>
                                <a href="" className={'sub-nav-text'}>About</a>
                                <a href="" className={'sub-nav-text'}>Blog</a>
                                <a href="" className={'sub-nav-text'}>Contact</a>
                            </div>
                            <div className="flex items-center gap-3 mx-4">
                                <Image src={Cart} alt={''}/>
                                <button
                                    className="px-2 py-1 font-bold font-small tracking-wide text-white capitalize transition-colors duration-300 transform bg-orange-600 hover:bg-orange-500 focus:outline-none focus:ring focus:ring-orange-300 focus:ring-opacity-80">
                                    0 Items
                                </button>
                                <Image src={Search} alt={''}/>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <nav className={'relative w-full max-w-full bg-[#C70039]'}>
                <div className="container w-3/4 h-3/4 mx-auto px-8 py-4">
                    <div className="flex gap-6">
                        <a href="#"
                           className={'sub-nav-text'}>Accessories</a>
                        <a href="#"
                           className={'sub-nav-text'}>Bags</a>
                        <a href="#"
                           className={'sub-nav-text'}>Fashion</a>
                        <a href="#"
                           className={'sub-nav-text'}>Footwear</a>
                        <a href="#"
                           className={'sub-nav-text'}>Lifestyle</a>
                        <a href="#"
                           className={'sub-nav-text'}>Outerwear</a>
                    </div>
                </div>
            </nav>
        </>
    )
}

export default NavigationBar;