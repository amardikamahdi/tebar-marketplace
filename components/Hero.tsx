import Image from "next/image";
import MainBanner from "@/public/Main-Banner.svg";

const Hero = () => {
    return (
        <>
            <Image className={'w-screen max-w-screen'} src={MainBanner} alt={''}/>
        </>
    )
}

export default Hero;